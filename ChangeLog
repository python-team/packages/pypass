CHANGES
=======

0.2.1
-----

* Create subdirectories if specified in password path (#14)
* Fix decrypted typo. (#12)
* Isolate gpg in tests (#13)
* setup.cfg: spaces around '='
* Check if hostname is None before return

0.2.0
-----

* Git commit on password edit
* Update setup.cfg

0.1.0
-----

* PEP8 fix
* Added error on unexisting password show
* Added connect to bash completion
* Added basic connect command
* Added hostname entry to get\_decrypted\_password
* Added EntryType enum to doc
* Added docs-requirements.txt for RTD
* Changed doc badge link
* Disabled installation of bash completion with setup.py
* Changed completion dir
* Adapted bash completion for pypass
* Imported pass.bash-completion
* requirements: enum -> enum34
* Refactored get\_decypted\_password: Now use EntryType enum
* Added bash completion in todo
* Refactored python package
* Flake8 fix
* Fixed flake8
* Added only\_usr and only\_pwd options to get\_decrypted\_password
* Updated todo
* Improved documentation
* Added help command
* Hide password during insert
* Fixed flake8
* python3 fixes
* Added generate\_password method to PasswordStore
* Fix xclip-related tests on Travis
* Added xclip to Travis
* Added -c, --clip option to show command
* Fixed typo
* Updated TODO
* Improved API documentation
* Added generate command to TODO
* Added grep to project status
* Added grep command
* Added grep command
* added test\_edit\_not\_exist
* Added edit command
* Support multi-line inserts
* Fixed wrong git work tree on 'pypass git [command]'
* .gpg-id should have a newline - fixed #3
* No longer alter env variables
* Added git\_init to PasswordStore
* Ask for confirmation on rm
* Find the right gpg binary
* fixed uses\_git
* Moved git\_add\_and\_commit to PasswordStore
* Added test\_init
* Rename: test\_git\_init\_clone -> test\_init\_clone
* Fixed issue on init
* Added test\_init
* Moved init from Command.py to PasswordStore
* Added test\_encrypt\_decrypt
* Added get\_decrypted\_password and insert\_password
* test\_get\_passwords\_list: fixed sorting issues
* Implemented get\_passwords\_list
* updated todo
* Removed docs-requirements.txt from Dockrefile
* Refactored tox.ini
* Added API autodoc
* document the API
* PasswordStore: Added test\_init
* fixed flake8
* Added cp command
* Mention that pypass will be a lib
* Fixed flake8
* Introduced PasswordStore class
* documented --clone
* Added test for init --clone
* Make tests verbose
* Override GIT\_DIR and GIT\_WORK\_TREE in init
* Fixed tests for PR#2
* Add init clone
* Commit after insert
* Configure git in travis
* Added tests for git init
* Fixed flake8
* Implemented git init
* Removed useless space
* Added info for documentation
* Added PASSWORD\_STORE\_GIT env var parameter
* Improved find and ls outputs
* Init outputs 'Password store initialized for [gpg-id]'
* Removed .gpg of find output
* Avoid capturing stderr
* Added git command
* Updated todo list
* gpg -> gpg2
* Manpage: pass -> pypass
* Manpage: pass -> pypass
* Added pypi version badge
* Added git examples to manpage
* imported simple examples from pass
* include readme in readthedocs
* Import pass manpage
* Fixed docs in Docker
* Changed readthedocs link
* Build docs in tox + fixed flake
* Added readthedocs badge
* Added docs-requiresments.txt for rtd
* Now build documentation and manpage
* Remove .gpg in ls output
* Added vim to Dockerfile
* Fixed issue on tree<1.5
* Fixed error on tree <1.7
* Added testing documentation
* Added mv command
* Dockerfile: Fixed python3 tests
* Added Dockerfile
* Added properties to setup.cfg
* Added more things in TODO
* Improved find pattern
* fixed typo
* Added find command
* Added subfolder argument to ls
* Added -r, --recursive options to rm
* Create dummy files in one line
* Invoke ls by default
* Removed useless lines
* Added rm command
* Added README to dist
* Fixed typo
* Update and rename README.md to README.rst
* added tree to travis
* cosmetic fix
* Added show command
* Added todo for pypyass insert
* gpg2 -> gpg
* Added show command
* Fixed pypy
* gpg --import-ownertrust
* Check for gpg errors
* Text py27 and py34 by default
* gpg\_id -> gpg-id + read gpg-id file
* fixed python3
* Added gpg key for testing
* Added gnupg to travis
* Removed debug strings
* Added pypass insert tests
* Progress on pypass insert
* Update README.md
* Update and rename README.rst to README.md
* Fixed tests with pypy
* flake8 fix
* Added travis and coveralls badges
* Changed README to .rst
* Fixed cover package
* Added test for init
* Create README.md
* Added init command
* Added pypass command
* Added setup.py
* Added tox.ini
* Added .travis.yml
* Initial commit
